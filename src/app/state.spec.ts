import { VideosArrived, videoListReducer, VideoAddedSuccess } from './state';
import { Video } from './types';
import { Action } from '@ngrx/store';

describe('The videoListReducer', () => {
  it('should add videos when they come from the backend', () => {
    const prev: Video[] = [], action = new VideosArrived([
      {
        author: 'asdasd',
        id: 'asdasd',
        title: 'asdasd',
        viewDetails: []
      },
      {
        author: 'hgjkgh',
        id: 'hjghj',
        title: 'asghjghjdasd',
        viewDetails: []
      },
      {
        author: 'jjh',
        id: 'asdasgghjd',
        title: 'ghddfg',
        viewDetails: []
      }
    ]);
    const newState = videoListReducer(prev, action);
    expect(newState).toEqual(action.videos);
    expect(newState).not.toBe(action.videos);
  });

  it('should preserve videos that were already in the list when getting new ones', () => {
    const prev: Video[] = [{
      author: 'asdasd',
      id: 'asdasd',
      title: 'ddsasd',
      viewDetails: []
    }], action = new VideosArrived([{
      author: 'asdasd',
      id: 'asdds',
      title: 'sasd',
      viewDetails: []
    }]);
    const result = videoListReducer(prev, action);
    expect(result.length).toEqual(2);
    expect(result).toContain(prev[0]);
    expect(result).toContain(action.videos[0]);
  });

  it('should add a video to the list when it is succesfully added to the backend', () => {
    const prev: Video[] = [],
      action = new VideoAddedSuccess({viewDetails: [], title: 'asdasd', id: 'asdasdds', author: 'asdasdad'});
    const result = videoListReducer(prev, action);
    expect(result).toContain(action.video);
    expect(result.length).toEqual(prev.length + 1);
  });

  it('should ignore any other events', () => {
    const prev: Video[] = [], action: Action = { type: 'asdasdasdasd' };
    expect(videoListReducer(prev, action)).toBe(prev);
  });
});
