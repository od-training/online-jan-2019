export interface View {
  age: number;
  date: string;
  region: string;
}

export interface Video {
  title: string;
  author: string;
  id: string;
  viewDetails: View[];
}
