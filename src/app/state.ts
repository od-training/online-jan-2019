import { Action } from '@ngrx/store';

import { Video } from './types';

const VIDEOS_ARRIVED = 'VIDEOS_ARRIVED';
export class VideosArrived implements Action {
  type = VIDEOS_ARRIVED;
  constructor(readonly videos: Video[]) { }
}

const VIDEO_ADDED_SUCCESS = 'VIDEO_ADDED_SUCCESS';
export class VideoAddedSuccess implements Action {
  type = VIDEO_ADDED_SUCCESS;
  constructor(readonly video: Video) { }
}

export const videoListReducer = function (videos: Video[] = [], action: Action): Video[] {
  switch (action.type) {
    case VIDEOS_ARRIVED:
      return [...videos, ...(action as VideosArrived).videos];
    case VIDEO_ADDED_SUCCESS:
      return [...videos, (action as VideoAddedSuccess).video];
    default:
      return videos;
  }
};
