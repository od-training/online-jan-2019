import { ElementFinder } from 'protractor';

export class ExamplePO {
  constructor(private el: ElementFinder) {}

  clickTheButton() {
    return this.el.$('button').click();
  }

  getMessage() {
    return this.el.$('span').getText();
  }
}
