import { browser, by, element } from 'protractor';
import { ExamplePO } from './example.po';

export class AppPage {

  exampleComponent = new ExamplePO(element(by.css('app-example')));

  navigateTo() {
    return browser.get('/');
  }

  getTitleText() {
    return element(by.css('app-root h1')).getText();
  }
}
