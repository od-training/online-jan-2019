import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Welcome to demoApp!');
  });

  it('should have a button that shows a message after it has been clicked', () => {
    page.navigateTo();
    expect(page.exampleComponent.getMessage()).toEqual('Booo!');
    page.exampleComponent.clickTheButton();
    expect(page.exampleComponent.getMessage()).toBe('You did it!');
  });
});
